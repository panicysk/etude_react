export {}

let bmi: (height: number, weight: number, printable?: boolean) => number = (
    height: number,
    weight: number,
    printable: boolean
): number => {
    const bmi: number = weight / (height * height)
    if (printable) {
        console.log("bmi")
        return -1
    } else {
        return bmi
    }
}

let bmi_true: number = bmi(1.71, 65, true)
let bmi_false: number = bmi(1.71, 65, false)

console.log('bmi true: ', bmi_true, 'bmi false: ', bmi_false)


