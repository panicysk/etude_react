export {}

type Pitchre1 = {
    throwingSpeed: number
}

type Batter1 = {
    battingAverage: number
}

const DaimaginSasaki: Pitchre1 = {
    throwingSpeed: 151
}

const OchiaiHirometsu: Batter1 = {
    battingAverage: 0.367
}

type TwoWayPlayer = Pitchre1 & Batter1

const OotaniSyouhei: TwoWayPlayer = {
    throwingSpeed: 165,
    battingAverage: 0.286
}

